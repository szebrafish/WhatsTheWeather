package com.zebra.whatstheweather;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    TextView textViewResults;
    EditText editTextCity;

    String html = "";

    public class DownloadTaskHtml extends AsyncTask<String, Void, String> {


        @Override
        protected String doInBackground(String... params) {

            String htmlSource = "";

            try {

                String htmlURL = "http://api.openweathermap.org/data/2.5/weather?q=" + params[0] + "&APPID=4f190f38ec2352fc0de13429a60f348b";
                URL url = new URL(htmlURL);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                InputStream inputStream = urlConnection.getInputStream();
                InputStreamReader reader = new InputStreamReader(inputStream);

                int data = reader.read();

                while (data != -1){

                    char current = (char) data;

                    htmlSource += current;

                    data = reader.read();
                }

                return htmlSource;

            } catch (MalformedURLException e) {
                e.printStackTrace();

            } catch (IOException e) {
                e.printStackTrace();

            }

            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            String cityName = "";
            String weatherHeading = "";
            String weatherDetails = "";
            String temperature = "";

            String weatherInfo = "";
            String mainInfo = "";

            try {

                JSONObject jsonFull = new JSONObject(result);

                cityName = jsonFull.getString("name");

                weatherInfo = jsonFull.getString("weather");
                mainInfo = jsonFull.getString("main");

                JSONArray weatherInfoArray = new JSONArray(weatherInfo);

                JSONObject mainInfoJson = new JSONObject(mainInfo);

                temperature = mainInfoJson.getString("temp");

                //JSONArray mainInfoArray = new JSONArray(mainInfo);

                for (int x = 0; x < weatherInfoArray.length(); x++){

                    JSONObject jsonPart = weatherInfoArray.getJSONObject(x);

                    weatherHeading = jsonPart.getString("main");
                    weatherDetails = jsonPart.getString("description");

                }

                /*for (int x = 0; x < mainInfoArray.length(); x++){

                    JSONObject jsonPart = mainInfoArray.getJSONObject(x);

                    temperature = jsonPart.getString("temp");

                }*/

            } catch (JSONException e) {
                e.printStackTrace();

            }

            Log.i("Full JSON", result);
            Log.i("weather", weatherInfo);

            setResultText(cityName, weatherHeading, weatherDetails, temperature);
        }
    }

    public void setResultText(String city, String heading, String description, String temp){

        double tempCelsiusInt = Double.valueOf(temp) - 273.15;

        String tempCelsiusString;

        if (tempCelsiusInt >= 0.0) {

            tempCelsiusString = Double.toString(tempCelsiusInt).substring(0, 3);

        } else {

            tempCelsiusString = Double.toString(tempCelsiusInt).substring(0, 4);

        }

        textViewResults.setText("City of " + city + "\n\n" + heading + ": " + description + "\n\nTemperature: " + tempCelsiusString + "\u2103");
    }

    public void clickButton (View view){

        //Hides on-screen keyboard (after input... when button is clicked ^^)
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(editTextCity.getWindowToken(), 0);

        DownloadTaskHtml downloadHtml = new DownloadTaskHtml();

        String enteredCity = editTextCity.getText().toString();

        if (!enteredCity.equals("")) {

            try {

                html = downloadHtml.execute(enteredCity).get();

            } catch (InterruptedException e) {
                e.printStackTrace();

            } catch (ExecutionException e) {
                e.printStackTrace();

            }
        } else{

            Toast.makeText(this, "Please enter a city to search", Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewResults = (TextView) findViewById(R.id.textViewResults);
        editTextCity = (EditText) findViewById(R.id.editTextCity);
    }
}
